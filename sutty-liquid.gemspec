# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'sutty-liquid'
  spec.version       = '0.12.3'
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'Liquid filters'
  spec.description   = 'An assorment of Liquid filters and tags for Jekyll used in Sutty'
  spec.homepage      = "https://0xacab.org/sutty/jekyll/#{spec.name}"
  spec.license       = 'GPL-3.0'
  spec.required_ruby_version = Gem::Requirement.new('> 2.7.0')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_dependency 'fast_blank', '~> 1.0'
  spec.add_dependency 'jekyll', '~> 4'
  spec.add_dependency 'rails-html-sanitizer', '~> 1.6.0'

  spec.add_development_dependency 'minitest', '~> 5.14'
  spec.add_development_dependency 'nokogiri'
  spec.add_development_dependency 'pry', '~> 0.13'
  spec.add_development_dependency 'rake', '~> 13'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'yard'
end
