# frozen_string_literal: true

require 'minitest/autorun'
require 'liquid'
require 'jekyll/filters/social_network'

class SocialNetworkTest < Minitest::Test
  include Jekyll::Filters::SocialNetwork

  def test_facebook_support
    network = social_network 'https://facebook.com/'

    assert_equal 'facebook', network['name']
  end

  def test_whatsapp_support
    network = social_network 'https://chat.whatsapp.com/'

    assert_equal 'whatsapp', network['name']
  end

  def test_telegram_support
    network = social_network 'https://t.me/'

    assert_equal 'telegram', network['name']
  end

  def test_non_url_support
    network = social_network 'invalid url'

    assert_equal 'globe', network['name']
  end

  def test_nil_url_support
    network = social_network nil

    assert_equal 'globe', network['name']
  end

  def test_some_other_url_support
    network = social_network 'https://www.random.social/network'

    assert_equal 'random', network['name']
  end
end
