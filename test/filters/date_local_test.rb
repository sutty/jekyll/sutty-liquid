# frozen_string_literal: true

require 'minitest/autorun'
require 'liquid'
require 'jekyll'
require 'jekyll/filters/date_local'

class DateLocalTest < Minitest::Test
  include Jekyll::Filters::DateLocal

  # Site stub
  def site
    conf = ::Jekyll.configuration('source' => Dir.pwd,
                                  'destination' => File.join(Dir.pwd, '_site'),
                                  'safe' => true,
                                  'watch' => false,
                                  'quiet' => true,
                                  'excerpt_separator' => '')

    @site ||= Jekyll::Site.new conf
    @site.data ||= Jekyll::DataReader.new(@site).read(@site.config['data_dir'])
    @context ||= Struct.new(:registers, keyword_init: true).new(registers: { site: @site })

    @site
  end

  def test_dates_are_localized
    site

    assert_equal ' 1 de Enero de 2021', date_local('2021-01-01', '%e de %B de %Y')
    assert_equal 'Vie VIE Viernes VIERNES Ene ENE Enero ENERO AM am',
                 date_local('2021-01-01 00:00:00', '%a %^a %A %^A %b %^b %B %^B %p %P')
    assert_equal 'Vie VIE Viernes VIERNES Ene ENE Enero ENERO PM pm',
                 date_local('2021-01-01 22:00:00', '%a %^a %A %^A %b %^b %B %^B %p %P')
  end

  def test_invalid_dates_are_returned_as_is
    site

    assert_nil date_local(nil, '')
    assert_equal '', date_local('', '')
    assert_equal 'nothing', date_local('nothing', '')
  end
end
