# sutty-liquid

Filters and tags used by Sutty.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'sutty-liquid'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sutty-liquid

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- sutty-liquid
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/sutty-liquid>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the sutty-liquid project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
