# frozen_string_literal: true

module Jekyll
  module Tags
    class Pry < Liquid::Tag
      # Pry into the rendering context
      #
      # Example usage:
      #
      # {% pry %}
      def render(context)
        require 'pry'

        binding.pry
      end
    end
  end
end

Liquid::Template.register_tag('pry', Jekyll::Tags::Pry)
