# frozen_string_literal: true

require 'jekyll/drops/url_drop'

module Jekyll
  module Drops
    module UrlDropDecorator
      # Permits to use layout as an URL placeholder
      def layout
        @obj.data['layout']
      end
    end
  end
end

Jekyll::Drops::UrlDrop.include Jekyll::Drops::UrlDropDecorator
