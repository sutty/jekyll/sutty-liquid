# frozen_string_literal: true

module Jekyll
  module Filters
    # Extracts numbers from a String, useful for converting phone
    # numbers to tel: links.
    #
    # Example usage:
    #
    # <a href="tel:{{ page.tel | numbers }}">{{ page.tel }}</a>
    #
    # @param [String]
    # @return [String]
    module Numbers
      def numbers(input)
        return unless input.respond_to? :gsub

        input.gsub(/[^0-9]/, '')
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Numbers)
