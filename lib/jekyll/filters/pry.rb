# frozen_string_literal: true

module Jekyll
  module Filters
    module Pry
      # Pry into the input and args
      #
      # Example usage:
      #
      # {{ site.posts | pry }}
      def pry(input, *args)
        require 'pry'

        binding.pry

        input
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Pry)
