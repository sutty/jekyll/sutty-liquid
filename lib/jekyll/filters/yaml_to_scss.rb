# frozen_string_literal: true

module Jekyll
  module Filters
    # YAML to SCSS
    module YamlToScss
      # Converts a YAML object into SCSS.  Only supports one level
      # values.
      #
      # Optionally give it an schema to filter out keys.
      #
      # You can add an array of keys to remove from the schema on
      # _config.yml, so if you're using a post to get an schema from,
      # some values don't end up on the SASS/SCSS file.
      #
      # @param :yaml [Hash,Jekyll::Drops::DocumentDrop]
      # @param :schema [Hash]
      # @return [String]
      def yaml_to_scss(yaml, schema = yaml)
        yaml = yaml.to_h if yaml.is_a? Jekyll::Drops::DocumentDrop

        unless yaml.is_a? Hash
          raise Liquid::ArgumentError, "#{yaml.inspect} needs to be a Hash" if @context.strict_filters

          return yaml
        end

        site = @context.registers[:site]
        keys = schema.keys
        reject_keys = site.config.dig('liquid', 'yaml_to_scss', 'reject_keys')

        if reject_keys.is_a?(Array) && !reject_keys.empty?
          keys = keys - reject_keys
        end

        yaml.slice(*keys).map do |key, value|
          next if value.nil?

          escape = schema[key].is_a?(Hash) && %w[image file].include?(schema.dig(key, 'type'))
          key = key_to_scss key

          case value
          when Array
            values = value.compact.map(&:to_s).reject(&:empty?).map do |v|
              escape ? v.dump : v
            end

            next if values.empty?

            "$#{key}: (#{values.join(',')},);"
          when Hash
            values =
              value.map do |k, v|
                next if v.nil?
                next if v.to_s.empty?

                v = v.dump if escape

                "\"#{key_to_scss k}\": #{v}"
              end.compact

            next if values.empty?

            "$#{key}: (#{values.join(',')},);"
          else
            next if value.to_s.empty?
            value = value.dump if escape

            "$#{key}: #{value};"
          end
        end.join("\n")
      end

      private

      # Dasherize key
      #
      # @param :key [String]
      # @return [String]
      def key_to_scss(key)
        key.to_s.tr '_', '-'
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::YamlToScss)
