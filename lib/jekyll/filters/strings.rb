# frozen_string_literal: true

module Jekyll
  module Filters
    module Strings
      # String#start_with?
      def start_with(input, start)
        input.to_s.start_with? start.to_s
      end

      # Replaces the last ocurrence of a String
      #
      # @param [String]
      # @param [String]
      # @param [String]
      # @return [String]
      def replace_last(input, search, replace)
        hay = input.split search
        needle = "#{replace}#{hay.pop}"

        hay.join(search) + needle
      end

      # We often use * for inclusive language, where language is
      # gendered.  This clashes with Commonmark's understanding of
      # emphasis delimiters.
      #
      # But we want to support inclusive language and we don't
      # necessarily need to know what a escape character is to do it.
      #
      # This filter disables *emphasis* in favor of _emphasis_ and
      # escapes them.
      #
      # @see {https://spec.commonmark.org/0.30/#emphasis-and-strong-emphasis}
      # @param :input [String]
      # @return [String]
      def allow_inclusive_language_in_markdown(input)
        return input if input.nil? || input.empty?
        return input unless input.to_s.include? '*'

        require 'securerandom'

        # Something that can't be accidentally replaced
        star_delimiter = "||#{SecureRandom.hex}||"
        escaped_delimiter = "||#{SecureRandom.hex}||"

        # 1. Exclude escaped stars
        #
        # 2. Exclude bold
        #
        # 3. Exclude list items
        #
        # 4. Escape remaining stars
        #
        # 5. Recover original stars
        input.to_s
             .gsub('\*', escaped_delimiter)
             .gsub('**', star_delimiter * 2)
             .gsub(/^(\s*)\*/, "\\1#{star_delimiter}")
             .gsub('*', '\*')
             .gsub(star_delimiter, '*')
             .gsub(escaped_delimiter, '\*')
      end

      def unescape(input)
        CGI.unescapeHTML input.to_s
      end

      # Escapes a string by percent encoding all reserved characters
      #
      # @param :input [Any]
      # @return [String]
      def component_escape(input)
        require 'addressable'

        @@component_escape ||= {}
        @@component_escape[input.to_s] ||= Addressable::URI.parse(input.to_s).normalize.to_s
      end

      # Sanitizes HTML. By default follows Sutty CMS allowlist.
      #
      # @todo Benchmark Jekyll cache, because it'll need to hash all params
      # @param input [String]
      # @param tags [String,Array<String>] Allowed elements. Can be a comma-separated string
      # @param attrs [String,Array<String>] Allowed attributes. Can be a comma-separated string
      def sanitize_html(input, tags = nil, attrs = nil)
        tags = list_to_array(tags, ALLOWED_TAGS)
        attrs = list_to_array(attrs, ALLOWED_ATTRIBUTES)

        sanitizer.sanitize(input.to_s.tr("\r", '').unicode_normalize, tags: tags, attributes: attrs).strip
      end

      ALLOWED_ATTRIBUTES = %w[style href src alt controls data-align data-multimedia data-multimedia-inner id name rel target referrerpolicy class colspan rowspan role data-turbo start type reversed].freeze
      ALLOWED_TAGS = %w[strong em del u mark p h1 h2 h3 h4 h5 h6 ul ol li img iframe audio video div figure blockquote figcaption a sub sup small table thead tbody tfoot tr th td br code].freeze

      private

      # Turn values to arrays or return a default array if empty. Use a
      # class-level cache because we'll probably parse tags and
      # attributes many times.
      #
      # @param input [String,Array<String>]
      # @param default [Array<String>]
      # @return [Array<String>]
      def list_to_array(input, default)
        array =
          case input
          when Array then input
          else input.to_s.split(',').map(&:strip)
          end

        if array.empty?
          default
        else
          array
        end
      end

      def sanitizer
        @@sanitizer ||=
          begin
            require 'loofah'
            require 'rails/html/sanitizer'
            require 'rails/html/scrubbers'

            Rails::HTML5::Sanitizer.safe_list_sanitizer.new
          end
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Strings)
