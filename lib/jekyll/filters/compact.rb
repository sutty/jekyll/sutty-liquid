# frozen_string_literal: true

module Jekyll
  module Filters
    module Compact
      # Removes nil values from an Array
      #
      # Example usage:
      #
      # {{ 'tag,,tag2' | split: ',' | compact }}
      #
      # @param [Array]
      # @return [Array]
      def compact(array)
        return [] unless array.is_a? Array

        @cache = Jekyll::Cache.new('Jekyll::Filters::Compact')

        @cache.getset(array.hash.to_s) do
          array.compact
        end
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Compact)
