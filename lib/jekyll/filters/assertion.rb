# frozen_string_literal: true

module Jekyll
  module Filters
    module Assertion
      # Compares two values.
      #
      # Example usage:
      #
      # {{ item.url | equals: page.url }}
      #
      # @param [Any]
      # @param [Any]
      # @return [TrueClass|FalseClass]
      def equals(input, comparison)
        input == comparison
      end

      # The two values must be different
      def not(input, comparison)
        input != comparison
      end

      # Greater than
      #
      # @param [Integer|Float]
      # @param [Integer|Float]
      # @return [TrueClass|FalseClass]
      def gt(input, comparison)
        input > comparison
      end

      # Less than
      #
      # @param [Integer|Float]
      # @param [Integer|Float]
      # @return [TrueClass|FalseClass]
      def lt(input, comparison)
        input < comparison
      end

      # Greater than or equal
      #
      # @param [Integer|Float]
      # @param [Integer|Float]
      # @return [TrueClass|FalseClass]
      def gte(input, comparison)
        input >= comparison
      end

      # Less than or equal
      #
      # @param [Integer|Float]
      # @param [Integer|Float]
      # @return [TrueClass|FalseClass]
      def lte(input, comparison)
        input <= comparison
      end

      # Ternary operator.  If the input value is truthy, return first
      # argument, otherwise returns the last.
      #
      # Example usage:
      #
      # {% assign active = item.url | equals: page.url %}
      # {{ active | ternary: 'active', '' }}
      #
      # @param [Any]
      # @param [Any]
      # @param [Any]
      # @return [Any]
      def ternary(input, value, alternative)
        if present(input) && input
          value
        else
          alternative
        end
      end

      # Returns the value when the input is not empty or falsey
      #
      # If tags have something:
      #
      # {{ post.tags | value_if: "some tags" }} => "some tags"
      def value_if(input, value)
        value if present(input) && input
      end

      # Returns the value when the input is empty or falsey
      #
      # If tags are empty:
      #
      # {{ post.tags | value_unless: "no tags" }} => nil
      def value_unless(input, value)
        value unless present(input) && input
      end

      # Returns the input when value is not empty and truthy
      #
      # {{ post.url | input_if: true }} => 'url/to/post/'
      def input_if(input, value)
        input if present(value) && value
      end

      # {{ post.url | input_unless: false }} => nil
      def input_unless(input, value)
        input unless present(value) && value
      end

      # Checks if input value is empty.
      #
      # {{ post.title | blank }} => false
      def blank(input)
        case input
        when TrueClass then false
        when FalseClass then true
        when NilClass then true
        when Integer then false
        when Float then false
        when Time then false
        when String
          require 'fast_blank'
          input.blank?
        when Array then input.compact.empty?
        when Hash then input.compact.empty?
        else input.respond_to?(:empty?) ? input.empty? : !!!input
        end
      end

      # The opposite of blank
      #
      # {{ post.title | present }} => true
      def present(input)
        !blank(input)
      end

      # Return the input if it's not blank
      #
      # {{ post.title | presence }} => 'the post title'
      def presence(input)
        input if present input
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Assertion)
