# frozen_string_literal: true

module Jekyll
  module Filters
    module Content
      HEADINGS = 'h1,h2,h3,h4,h5,h6'

      # Add id attribute to all headings with an optional prefix.  It
      # also creates a table of contents with unique IDs.
      #
      # It doesn't change pre-existing IDs.
      #
      # @example Render content with unique IDs
      #   {{ content | canonicalize_headings }}
      # @param input [String] HTML string
      # @param prefix [nil,String] Prefix IDs with this
      # @return [String]
      def canonicalize_headings(input, prefix = nil)
        @@canonicalize_headings ||= {}
        @@canonicalize_headings[input.hash] ||=
          begin
            require 'nokogiri'

            toc = toc(input)

            html_fragment(input).tap do |html|
              html.css(HEADINGS).each do |h|
                id = h['id']

                unless id
                  slug = ::Jekyll::Utils.slugify(h.text, mode: 'pretty')
                  id   = unique_id(slug, prefix, toc)
                  h['id'] = id
                end

                toc[id] = {
                  'level' => h.name[1].to_i,
                  'title' => h.text,
                  'id' => id
                }
              end
            end.to_s
          end
      end

      # Extracts a table of contents from HTML up to a certain headings
      # level.
      #
      # It returns an array of hashes with title, level and id.
      #
      # @example Create a table of contents
      #   {% assign toc = content | table_of_contents: 2 %}
      #
      #   {% unless toc == empty %}
      #     {% for item in toc %}
      #       {% assign heading = "h" | append: item.level %}
      #       <{{ heading }}>
      #         <a href="#{{ item.id }}">
      #           {{ item.title }}
      #         </a>
      #       </{{ heading }}>
      #     {% endfor %}
      #   {% endunless %}
      # @param input [String]
      # @param max_level [Integer] All headings up to this level
      # @param prefix [String,nil] Prefix
      # @return [Hash]
      def table_of_contents(input, max_level = 2, prefix = nil)
        canonicalize_headings(input, prefix)

        toc(input).select do |_id, item|
          item['level'] <= max_level
        end.values
      end

      private

      # Parses and caches an HTML5 fragment
      #
      # @param input [String]
      # @return [Nokogiri::HTML5::Fragment]
      def html_fragment(input)
        @@html_fragment ||= {}
        @@html_fragment[input.hash] ||= Nokogiri::HTML5.fragment(input.to_s)
      end

      # @param input [String]
      # @return [Hash]
      def toc(input)
        @@toc ||= {}
        @@toc[input.hash] ||= {}
      end

      # Apply a prefix to an ID unless it's already applied
      #
      # @param id [String]
      # @param prefix [nil,String]
      # @return [String]
      def prefix_id(id, prefix)
        return id if prefix.nil?
        return id if id.start_with? "#{prefix}-"

        "#{prefix}-#{id}"
      end

      # Obtains a unique ID for TOC
      #
      # @param id [String]
      # @param prefix [String,nil]
      # @param toc [Hash]
      # @return [String]
      def unique_id(id, prefix, toc)
        prefixed_id = prefix_id id, prefix

        return prefixed_id unless toc.key? prefixed_id

        require 'securerandom'

        loop do
          unique = "#{prefixed_id}-#{SecureRandom.hex(3)}"

          break unique unless toc.key? unique
        end
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Content)
