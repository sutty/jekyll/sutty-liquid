# frozen_string_literal: true

module Jekyll
  module Filters
    module Menu
      # Verifies if the given URL is an active item on the menu.
      #
      # Example usage:
      #
      # {% assign active_url = page.url | menu_active %}
      # {% for item in site.i18n.menu.items %}
      #   <a
      #     href="{{ item.href }}"
      #     class="{{ page.url | menu_active | equals: item.href | ternary: 'active', '' }}">
      #
      #     {{ item.title }}
      #   </a>
      # {% endfor %}
      #
      # @param [String] The URL to be verified
      # @return [String] The item URL
      def menu_active_item(page_url)
        site_menu&.find do |key, _value|
          key == page_url || page_url.start_with?(key)
        end&.last
      end

      # The menu is defined in a data file that corresponds to the site
      # language.  This method converts the menu items into a map of URL
      # parts and actual URLs.
      #
      # @see _data/es.yml
      # @see {https://0xacab.org/sutty/jekyll/jekyll-locales}
      # @return [Hash]
      def site_menu
        site = @context.registers[:site]
        @site_menu ||= site.data.dig(site.config['locale'], 'menu', 'items')&.reduce({}) do |menu, item|
          # If the item has a layout, we pick the first post and update
          # the href.  We can't do the same for all posts because we
          # wouldn't be able to cache the menu.
          if item['layout']
            doc = site.documents.find do |doc|
              doc.data['layout'] == item['layout']
            end

            item['href'] = doc&.url
          end

          # Ignore empty or anchor items
          next menu if item['href'].nil? || item['href']&.empty? || item['href']&.start_with?('#')

          menu[item['href']] = item['href']

          item['active']&.each do |a|
            menu[a] = item['href']
          end

          menu
        end
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Menu)
