# frozen_string_literal: true

module Jekyll
  module Filters
    module SocialNetwork
      MASTODON   = %r{\A/@\w+\z}.freeze
      NON_MASTODON = %w[youtube.com tiktok.com]
      SUBDOMAINS = /\A(www|chat)\./.freeze
      DOMAIN_MAP = {
        't.me' => 'telegram',
        'youtu.be' => 'youtube',
        'open.spotify.com' => 'spotify'
      }.freeze

      # Takes a URL and returns a Hash of attributes, useful when you
      # want to generate social network buttons from an undetermined
      # list of URLs.
      #
      # Example usage:
      #
      # {% assign mastodon = 'https://todon.nl/@sutty' | social_network %}
      # <a href="{{ mastodon.url }}">
      #   <i class="fa-{{ mastodon.name }}"></i>
      #
      #   {{ mastodon.name | capitalize }}
      # </a>
      #
      # @param [String]
      # @return [Hash]
      def social_network(url)
        begin
          require 'uri'

          uri  = URI url
          host = uri.host = uri.host.sub SUBDOMAINS, ''

          name   = DOMAIN_MAP[host]
          name ||= mastodon?(uri) ? 'mastodon' : host.split('.', 2).first
        rescue ArgumentError, URI::InvalidURIError => e
          Jekyll.logger.warn(e.message) if Jekyll.respond_to?(:logger)
        ensure
          host ||= url
          name ||= 'globe'
        end

        { 'host' => host, 'name' => name, 'url' => url }.to_liquid
      end

      private

      # Other social networks use @username on the URL too
      def mastodon?(uri)
        return false if NON_MASTODON.include? uri.host

        MASTODON =~ uri.path
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::SocialNetwork)
