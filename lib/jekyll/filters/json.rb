# frozen_string_literal: true

module Jekyll
  module Filters
    module Json
      # Converts an object to JSON
      #
      # Note: Jekyll 4 has a jsonify filter now.
      #
      # @param [Any]
      # @return [String]
      def json(object)
        require 'json'

        JSON.dump object
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Json)
