# frozen_string_literal: true

module Jekyll
  module Filters
    module File
      # File extension.
      #
      # Example usage:
      #
      # {{ page.file.path | extname }}
      #
      # @param [String]
      # @return [Nil|String]
      def extname(input)
        return unless ::File.exist? input.to_s

        ::File.extname input
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::File)
